package robolejos.robolejosmobileandroid;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.app.Fragment;
import android.support.design.widget.TabLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;

import org.reactivestreams.Subscription;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import robolejos.robolejosmobileandroid.adapters.SubjectAdapter;
import robolejos.robolejosmobileandroid.entities.Category;
import robolejos.robolejosmobileandroid.entities.Subject;
import robolejos.robolejosmobileandroid.logging.LoggingTags;
import robolejos.robolejosmobileandroid.services.ServiceProvider;
import robolejos.robolejosmobileandroid.services.category.ICategoryService;
import robolejos.robolejosmobileandroid.services.subject.ISubjectService;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link Forum#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Forum extends Fragment {

    private TabLayout categoryMenu;
    private ListView subjectListView;
    private LinearLayout contentLayout;
    private LinearLayout loadingLayout;

    private ISubjectService subjectService;
    private ICategoryService categoryService;

    private List<Category> categories;

    private OnForumSubjectSelecten onForumSubjectSelecten;

    private Disposable subjectsSubscription;
    private Disposable categorySubscription;

    public Forum() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment Forum.
     */
    // TODO: Rename and change types and number of parameters
    public static Forum newInstance() {
        Forum fragment = new Forum();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_forum, container, false);

        this.categoryMenu = (TabLayout) view.findViewById(R.id.GenreMenu);
        this.subjectListView = (ListView) view.findViewById(R.id.SubjectList);
        this.contentLayout = (LinearLayout) view.findViewById(R.id.contentLayout);
        this.loadingLayout = (LinearLayout) view.findViewById(R.id.loadingLayout);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.categoryMenu.setTabMode(TabLayout.MODE_SCROLLABLE);
        this.categoryMenu.setSelectedTabIndicatorColor(Color.BLUE);
        this.subjectService = ServiceProvider.getInstance().SubjectService;
        this.categoryService = ServiceProvider.getInstance().CategoryService;
        categories = new ArrayList<Category>();

        //Set the page to be loading.
        loading(true);

        getCategories();
        setSubjectListViewListener();
        setCategoryMenuListener();
    }

    /**
     * Sets a OnItemClickListener for th fragment listview.
     * Calls the onForumSubjectSelected interface for fragment replacement.
     */
    private void setSubjectListViewListener(){
        subjectListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object object = parent.getItemAtPosition(position);
                Subject subject = (Subject) object;
                if(subject != null){
                    onForumSubjectSelecten.onSubjectSeleccted(subject.uid);
                }
            }
        });
    }

    /**
     * Sets a listener for the fragment tablayout.
     * The All tabs is default selected resultet in refreshTab being called instant.
     */
    private void setCategoryMenuListener(){
        categoryMenu.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener(){

            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                refreshTab(tab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    /**
     * Calls the selected tab ides (All as default).
     * gets the category according to the index from categories.
     * Retrives subjects belonging to the category, calls populateList with the retrived list of subjects
     * @param tab
     */
    public void refreshTab(TabLayout.Tab tab){
        int index = tab.getPosition();
        Observable<List<Subject>> observable;
        Log.e("TabIndex", "" + index);

        if(index == 0){
            observable = subjectService.getSubjects();
        }
        else{
            observable = subjectService.getSelectedSubjects(categories.get(index - 1).uid);
        }

        //Set the page to be loading.
        loading(true);

        this.subjectsSubscription = observable.subscribe(new Consumer<List<Subject>>() {
            @Override
            public void accept(@NonNull List<Subject> subjects) throws Exception {
                populateList(subjects);
                loading(false);
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(@NonNull Throwable e) throws Exception {
                Log.e(LoggingTags.FORUM, e.getMessage());
                loading(false);
            }
        });
    }

    private void loading(boolean state) {
        if (state) {
            this.contentLayout.setVisibility(View.INVISIBLE);
            this.loadingLayout.setVisibility(View.VISIBLE);
        } else {
            this.loadingLayout.setVisibility(View.INVISIBLE);
            this.contentLayout.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Populates the list of subjects with the given list of subjects.
     * The list uses a custem adapter for custom gui design
     * @param subjects
     */
    private void populateList(List<Subject> subjects) {
        final Fragment fragment = this;

        SubjectAdapter adapter = new SubjectAdapter(fragment.getContext(), R.layout.resource_subjectitem, subjects);
        subjectListView.setAdapter(adapter);
    }

    /**
     * Loads categories from firebase
     */
    private void getCategories(){
        this.categorySubscription = this.categoryService.getAllCategories().subscribe(new Consumer<List<Category>>() {
            @Override
            public void accept(@NonNull List<Category> c) throws Exception {
                categories = c;
                populateCategoryMenu();
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(@NonNull Throwable e) throws Exception {
                Log.e(LoggingTags.FORUM, e.getMessage());
            }
        });
    }

    /**
     * populates Categorymenu (tablayout) with the categories list
     */
    private void populateCategoryMenu() {
        TabLayout.Tab AllSubjects = this.categoryMenu.newTab();

        AllSubjects.setText("All");
        this.categoryMenu.addTab(AllSubjects);
        Log.e("CategoryCount2", ""+ categories.size());

        for (Category category : categories){

            TabLayout.Tab tab = this.categoryMenu.newTab();
            tab.setText(category.title);
            this.categoryMenu.addTab(tab);
        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnForumSubjectSelecten){
            this.onForumSubjectSelecten = (OnForumSubjectSelecten) context;
        }
    }
    @Override
    public void onDetach() {
        super.onDetach();
        if (this.subjectsSubscription != null && !this.subjectsSubscription.isDisposed())
            this.subjectsSubscription.dispose();
        if (this.categorySubscription != null && !this.categorySubscription.isDisposed())
            this.categorySubscription.dispose();
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnForumSubjectSelecten {
        // TODO: Update argument type and name
        void onSubjectSeleccted(String subjectUid);
    }
}
