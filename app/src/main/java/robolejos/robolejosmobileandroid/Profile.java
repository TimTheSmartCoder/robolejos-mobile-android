package robolejos.robolejosmobileandroid;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.w3c.dom.Text;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import robolejos.robolejosmobileandroid.entities.User;
import robolejos.robolejosmobileandroid.logging.LoggingTags;
import robolejos.robolejosmobileandroid.services.ServiceProvider;
import robolejos.robolejosmobileandroid.services.auth.IAuthService;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Profile.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Profile#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Profile extends Fragment {

    /**
     * Views references.
     */
    private TextView usernameTextView;
    private TextView emailTextView;

    /**
     * Services.
     */
    private IAuthService authService;

    private User user;

    public Profile() { }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment Profile.
     */
    public static Profile newInstance() {
        Profile fragment = new Profile();
        Bundle args = new Bundle();
        //args.putSerializable(ARG_USER, user);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            //this.user = (User)getArguments().getSerializable(ARG_USER);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_profile, container, false);

        //Get components.
        this.usernameTextView = (TextView) view.findViewById(R.id.usernameTextView);
        this.emailTextView = (TextView) view.findViewById(R.id.emailTextView);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Get services.
        this.authService = ServiceProvider.getInstance().AuthService;

        this.authService.getCurrentUser().subscribe(new Observer<User>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) { }

            @Override
            public void onNext(@NonNull User currentUser) {
                user = currentUser;
                usernameTextView.setText(user.profile.username);
                emailTextView.setText(user.profile.email);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.e(LoggingTags.PROFILE, e.getMessage());
            }

            @Override
            public void onComplete() { }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
