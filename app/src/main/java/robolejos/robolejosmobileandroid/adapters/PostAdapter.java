package robolejos.robolejosmobileandroid.adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import robolejos.robolejosmobileandroid.R;
import robolejos.robolejosmobileandroid.entities.Post;

/**
 * Created by Morten on 12-05-2017.
 */

public class PostAdapter extends ArrayAdapter<Post> {

    private int layoutResource;
    private Post post;
    private int max = 30;

    public PostAdapter(Context context, int layoutResouce, List<Post> subjectsList){
        super(context, layoutResouce, subjectsList);
        this.layoutResource = layoutResouce;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View view =convertView;


        if(view == null){
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            view = layoutInflater.inflate(layoutResource, null);
        }

        post = getItem(position);

        if(post != null){
            TextView postTitle = (TextView) view.findViewById(R.id.title);
            TextView postContent = (TextView) view.findViewById(R.id.content);
            TextView postUsername = (TextView) view.findViewById(R.id.username);

            if(position %2 == 0){
                postTitle.setBackgroundColor(Color.LTGRAY);
                postContent.setBackgroundColor(Color.LTGRAY);
                postUsername.setBackgroundColor(Color.LTGRAY);
            } else {
                postTitle.setBackgroundColor(Color.WHITE);
                postContent.setBackgroundColor(Color.WHITE);
                postUsername.setBackgroundColor(Color.WHITE);
            }

            if(postTitle != null){
                postTitle.setText(post.info.title);
            }
            if(postContent != null){
                postContent.setText(post.info.content);
            }
            if(postUsername != null){
                postUsername.setText(post.info.username);
            }
        }
        return view;
    }

    public Post getPost(){
        return post;
    }
}
