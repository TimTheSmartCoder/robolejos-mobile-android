package robolejos.robolejosmobileandroid.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import robolejos.robolejosmobileandroid.R;
import robolejos.robolejosmobileandroid.entities.Subject;

/**
 * Created by Morten on 08-05-2017.
 */

public class SubjectAdapter extends ArrayAdapter<Subject> {

    private int layoutResource;
    private Subject subject;

    public SubjectAdapter(Context context, int layoutResouce, List<Subject> subjectsList){
        super(context, layoutResouce, subjectsList);
        this.layoutResource = layoutResouce;
    }

    /**
     * goes trough the given list in the constructer
     * and sets the values to its given resoucce and the views within that.
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View view =convertView;

        if(view == null){
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            view = layoutInflater.inflate(layoutResource, null);
        }

        subject = getItem(position);

        if(subject != null){
            TextView subjectTittle = (TextView) view.findViewById(R.id.tittleSubject);
            TextView subjectCreator = (TextView) view.findViewById(R.id.descriptionSubject);
            LinearLayout contentLayout = (LinearLayout) view.findViewById(R.id.contentLayout);

            if(position %2 == 0){
                //subjectTittle.setBackgroundColor(Color.LTGRAY);
                //subjectCreator.setBackgroundColor(Color.LTGRAY);
                contentLayout.setBackgroundColor(Color.LTGRAY);
            }

            if(subjectTittle != null){
                subjectTittle.setText(subject.title);
            }

            if(subjectCreator != null){
                subjectCreator.setText(subject.description);
            }
        }
        return view;
    }

    public Subject getSubject(){
        return subject;
    }
}
