package robolejos.robolejosmobileandroid.adapters;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.List;

import robolejos.robolejosmobileandroid.R;
import robolejos.robolejosmobileandroid.entities.Reply;
import robolejos.robolejosmobileandroid.entities.Subject;

/**
 * Created by Tim17 on 12-05-2017.
 */
public class ReplyAdapter extends ArrayAdapter<Reply> {

    private int layoutResource;
    private Reply reply;

    public ReplyAdapter(Context context, int layoutResouce, List<Reply> replyList){
        super(context, layoutResouce, replyList);
        this.layoutResource = layoutResouce;
    }

    /**
     * goes trough the given list in the constructer
     * and sets the values to its given resoucce and the views within that.
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        View view =convertView;

        if(view == null){
            LayoutInflater layoutInflater = LayoutInflater.from(getContext());
            view = layoutInflater.inflate(layoutResource, null);
        }

        reply = getItem(position);

        if(reply != null){
            TextView usernameTextView = (TextView) view.findViewById(R.id.usernameTextView);
            EditText contentEditText = (EditText) view.findViewById(R.id.contentEditText);
            ScrollView scrollView = (ScrollView) view.findViewById(R.id.replyScrollView);

            if (usernameTextView != null)
                usernameTextView.setText(reply.info.username);

            if (contentEditText != null)
                contentEditText.setText(reply.info.content);

            if (position % 2 == 0) {
                scrollView.setBackgroundColor(Color.LTGRAY);
                usernameTextView.setBackgroundColor(Color.LTGRAY);
            } else {
                scrollView.setBackgroundColor(Color.WHITE);
                usernameTextView.setBackgroundColor(Color.WHITE);
            }
        }
        return view;
    }

    public Reply getReply(){
        return reply;
    }
}