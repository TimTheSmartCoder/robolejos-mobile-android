package robolejos.robolejosmobileandroid.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.media.Image;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tim17 on 09-05-2017.
 */

public class ImageGridViewAdapter extends BaseAdapter {

    /**
     * Context of the Owner.
     */
    private Context mContext;

    /**
     * List of images the Adapter will display.
     */
    private List<Bitmap> images;

    /**
     * Constructs ImageGridViewAdapter.
     * @param c - Context.
     */
    public ImageGridViewAdapter(Context c) {
        mContext = c;
        this.images = new ArrayList<>();
    }

    /**
     * Adds the given uri to the list of images.
     * @param uri
     */
    public void addImage(Bitmap uri) {
        this.images.add(uri);
        notifyDataSetChanged();
    }

    public List<Bitmap> getImages() {
        return this.images;
    }

    /**
     * Removes the image at the position.
     * @param position - Position of the images to remove.
     */
    public void removeImage(final int position) {
        this.images.remove(position);
        notifyDataSetChanged();
    }

    public void clear() {
        this.images.clear();
        notifyDataSetChanged();
    }

    public int getCount() {
        return images.size();
    }

    public Bitmap getItem(int position) {
        return this.images.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    // create a new ImageView for each item referenced by the Adapter
    public View getView(final int position, View convertView, ViewGroup parent) {
        ImageView imageView;
        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            imageView = new ImageView(mContext);
            imageView.setLayoutParams(new GridView.LayoutParams(85, 85));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8, 8, 8, 8);

        } else {
            imageView = (ImageView) convertView;
        }
        Log.e("ImageView", "Setting image uri!!!!");
        imageView.setImageBitmap(images.get(position));
        return imageView;
    }
}
