package robolejos.robolejosmobileandroid;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.vansuita.pickimage.bean.PickResult;
import com.vansuita.pickimage.bundle.PickSetup;
import com.vansuita.pickimage.dialog.PickImageDialog;
import com.vansuita.pickimage.listeners.IPickResult;

import java.util.List;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import robolejos.robolejosmobileandroid.adapters.ImageGridViewAdapter;
import robolejos.robolejosmobileandroid.entities.Category;
import robolejos.robolejosmobileandroid.entities.Subject;
import robolejos.robolejosmobileandroid.logging.LoggingTags;
import robolejos.robolejosmobileandroid.services.ServiceProvider;
import robolejos.robolejosmobileandroid.services.category.ICategoryService;
import robolejos.robolejosmobileandroid.services.post.IPostService;
import robolejos.robolejosmobileandroid.services.subject.ISubjectService;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link Post#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Post extends Fragment implements View.OnClickListener {

    //Services.
    private ICategoryService categoryService;
    private ISubjectService subjectService;
    private IPostService postService;

    //Subscriptions.
    private Disposable categorySubscription;
    private Disposable subjectSubscription;
    private Disposable postSubcription;

    //Reference to components.
    private Button imageButton;
    private ImageView imageView;
    private GridView imageGridView;
    private Button imageClearButton;
    private Spinner categorySpinner;
    private Spinner subjectSpinner;
    private Button submitButton;
    private EditText titleEditText;
    private EditText contentEditText;
    private TextView errorTextView;
    private LinearLayout contentLayout;
    private LinearLayout loadingLayout;

    private String subjectUid;
    private OnPostCreated onPostCreated;


    public Post() { }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment Post.
     */
    // TODO: Rename and change types and number of parameters
    public static Post newInstance() {
        Post fragment = new Post();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_post, container, false);

        this.imageButton = (Button) view.findViewById(R.id.imageButton);
        this.imageView = (ImageView) view.findViewById(R.id.imageView);
        this.imageGridView = (GridView) view.findViewById(R.id.imageGridView);
        this.imageClearButton = (Button) view.findViewById(R.id.imageClearButton);
        this.categorySpinner = (Spinner) view.findViewById(R.id.categorySpinner);
        this.subjectSpinner = (Spinner) view.findViewById(R.id.subjectSpinner);
        this.submitButton = (Button) view.findViewById(R.id.submitButton);
        this.titleEditText = (EditText) view.findViewById(R.id.titleTextView);
        this.contentEditText = (EditText) view.findViewById(R.id.postContentTextView);
        this.contentLayout = (LinearLayout) view.findViewById(R.id.contentLayout);
        this.loadingLayout = (LinearLayout) view.findViewById(R.id.loadingLayout);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnPostCreated){
            this.onPostCreated = (OnPostCreated) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

        if (this.categorySubscription != null && !this.categorySubscription.isDisposed())
            this.categorySubscription.dispose();
        if (this.subjectService != null && !this.subjectSubscription.isDisposed())
            this.subjectSubscription.dispose();
        if(this.postSubcription != null && !this.postSubcription.isDisposed())
            this.postSubcription.dispose();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        this.categoryService = ServiceProvider.getInstance().CategoryService;
        this.subjectService = ServiceProvider.getInstance().SubjectService;
        this.postService = ServiceProvider.getInstance().PostService;

        this.imageButton.setOnClickListener(this);
        this.imageClearButton.setOnClickListener(this);
        this.imageGridView.setAdapter(new ImageGridViewAdapter(this.getContext()));
        this.subjectSpinner.setEnabled(false);
        this.submitButton.setOnClickListener(this);

        //Set the onItemSelected listener to dispatch fetch for subjects under the
        //selected category.
        initializeCategorySpinner();

        //Fetch from the category service.
        fetchCategories();

        setSubjectListener();
    }

    public void fetchCategories() {
        this.categorySubscription
                = this.categoryService.getAllCategories().subscribe(new Consumer<List<Category>>() {
            @Override
            public void accept(@NonNull List<Category> categories) throws Exception {
                setCategorySpinner(categories);
            }
        });
    }

    public void initializeCategorySpinner() {
        this.categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Category category = (Category) parent.getItemAtPosition(position);
                subjectSubscription
                        = subjectService.getSelectedSubjects(category.uid)
                        .subscribe(new Consumer<List<Subject>>() {
                            @Override
                            public void accept(@NonNull List<Subject> subjects) throws Exception {
                                setSubjectSpinner(subjects);
                                subjectSpinner.setEnabled(true);
                            }
                        });
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });
    }

    public void setCategorySpinner(List<Category> categories) {
        //Create default adapter.
        ArrayAdapter<Category> categoryAdapter = new ArrayAdapter<Category>(
                this.getContext(),
                android.R.layout.simple_spinner_dropdown_item,
                categories
        );
        this.categorySpinner.setAdapter(categoryAdapter);
    }

    public void setSubjectSpinner(List<Subject> subjects) {
        //Create default adapter.
        ArrayAdapter<Subject> subjectAdapter = new ArrayAdapter<Subject>(
                this.getContext(),
                android.R.layout.simple_spinner_dropdown_item,
                subjects
        );
        this.subjectSpinner.setAdapter(subjectAdapter);
    }

    private void setSubjectListener() {
        this.subjectSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Object object = parent.getItemAtPosition(position);
                Subject subject = (Subject) object;
                subjectUid = subject.uid;
                subjectSpinner.setSelection(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent resultIntent) {
        super.onActivityResult(requestCode, resultCode, resultIntent);
        Log.e("Image", "activity post");
    }

    public void imageChoose(View view) {
        PickImageDialog.build(new PickSetup()).setOnPickResult(new IPickResult() {
            @Override
            public void onPickResult(PickResult r) {
                if (r.getError() == null) {
                    ((ImageGridViewAdapter) imageGridView.getAdapter()).addImage(r.getBitmap());
                }
                else
                    Log.e("Image", "error loading image.");
            }
        }).show((AppCompatActivity)this.getActivity());
    }

    /**
     * OnClick method for handling the event from the
     * layout. Normally it will be send to the Activity,
     * but in our case it will not cause we attach this
     * function through code and it will when call the
     * correct method.
     * @param v - View calling.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageButton:
                this.imageChoose(v);
                break;
            case R.id.imageClearButton:
                this.clearImagsChoosed(v);
                break;
            case R.id.submitButton:
                this.onSubmit(v);
                break;
        }
    }

    private void loading(boolean state) {
        if (state) {
            this.contentLayout.setVisibility(View.INVISIBLE);
            this.loadingLayout.setVisibility(View.VISIBLE);
        } else {
            this.loadingLayout.setVisibility(View.INVISIBLE);
            this.contentLayout.setVisibility(View.VISIBLE);
        }
    }

    private void onSubmit(View v) {
        String title = this.titleEditText.getText().toString();
        String content = this.contentEditText.getText().toString();
        List<Bitmap> images = ((ImageGridViewAdapter) this.imageGridView.getAdapter()).getImages();

        if (title.length() <= 0) {
            this.titleEditText.setError(getText(R.string.post_error_no_title));
            return;
        }

        if (content.length() <= 0) {
            this.contentEditText.setError(getText(R.string.post_error_no_content));
            return;
        }

        loading(true);

        this.postSubcription = postService.createPost(content, title, this.subjectUid).subscribe(new Consumer<String>() {
            @Override
            public void accept(final @NonNull String s) throws Exception {
                List<Bitmap> images = ((ImageGridViewAdapter) imageGridView.getAdapter()).getImages();
                if (images.size() != 0) {
                    postService.uploadImages(images, s).subscribe(new Consumer<Boolean>() {
                        @Override
                        public void accept(@NonNull Boolean aBoolean) throws Exception {
                            loading(false);
                            onPostCreated.onPostCreatedSeleccted(s);
                        }
                    });
                } else {
                    loading(false);
                    onPostCreated.onPostCreatedSeleccted(s);
                }
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(@NonNull Throwable throwable) throws Exception {
                loading(false);
                Log.e(LoggingTags.POST, throwable.getMessage());
            }
        });
    }

    public void clearImagsChoosed(View view) {
        ((ImageGridViewAdapter) imageGridView.getAdapter()).clear();
    }

    public interface OnPostCreated {
        // TODO: Update argument type and name
        void onPostCreatedSeleccted(String postuid);
    }
}
