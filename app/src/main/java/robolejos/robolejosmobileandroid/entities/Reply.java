package robolejos.robolejosmobileandroid.entities;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Tim17 on 12-05-2017.
 */

public class Reply {

    public static class Info {
        public Info() { }
        public String content;
        public String userUid;
        public String username;
    }

    public Reply() { this.info = new Info(); }
    public String uid;
    public long created;
    public Info info;
}
