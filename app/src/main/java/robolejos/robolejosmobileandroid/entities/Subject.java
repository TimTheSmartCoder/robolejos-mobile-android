package robolejos.robolejosmobileandroid.entities;

import android.media.Image;

/**
 * Created by Morten on 28-04-2017.
 */

public class Subject {
    public String title;
    public String description;
    public String uid;


    public Subject(){}

    @Override
    public String toString() {
        return this.title;
    }

}
