package robolejos.robolejosmobileandroid.entities;

import java.io.Serializable;

/**
 * Created by Tim17 on 05-05-2017.
 */

public class User implements Serializable {

    public static class Profile {
        public String email;
        public String username;
    }

    public Profile profile;

    public String uid;

    /**
     * Default constructor neccasary because firebase used to read
     * and write properties.
     */
    public User() {
        this.profile = new Profile();
    }
}
