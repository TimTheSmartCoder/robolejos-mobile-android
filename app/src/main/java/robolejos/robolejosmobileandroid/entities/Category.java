package robolejos.robolejosmobileandroid.entities;

/**
 * Created by Morten on 09-05-2017.
 */

public class Category {
    public String description;
    public String title;
    public String uid;

    public Category(String description, String title, String uid){
        this.description = description;
        this.title = title;
        this.uid = uid;
    }

    public Category(){}

    public String getDescription(){return this.description;}
    public String getTitle(){return this.title;}
    public String getUid(){return this.uid;}

    @Override
    public String toString() {
        return this.title;
    }
}
