package robolejos.robolejosmobileandroid.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Morten on 10-05-2017.
 */

public class Post {

    public static class Info {
        public Info() { }
        public String content;
        public String subjectUid;
        public String title;
        public String userUid;
        public String username;
        public List<String> images;
    }

    public Info info;
    public String uid;
    public long created;

    public Post(){
        this.info = new Info();
    }
}
