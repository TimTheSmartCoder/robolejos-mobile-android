package robolejos.robolejosmobileandroid.services;

import robolejos.robolejosmobileandroid.services.auth.IAuthService;
import robolejos.robolejosmobileandroid.services.auth.firebase.FirebaseAuthService;
import robolejos.robolejosmobileandroid.services.category.ICategoryService;
import robolejos.robolejosmobileandroid.services.category.firebase.FirebaseCategoryService;
import robolejos.robolejosmobileandroid.services.post.IPostService;
import robolejos.robolejosmobileandroid.services.post.firebase.FirebasePostService;
import robolejos.robolejosmobileandroid.services.reply.IReplyService;
import robolejos.robolejosmobileandroid.services.reply.firebase.FirebaseReplyService;
import robolejos.robolejosmobileandroid.services.storage.IStorageService;
import robolejos.robolejosmobileandroid.services.storage.firebase.FirebaseStorageService;
import robolejos.robolejosmobileandroid.services.subject.ISubjectService;
import robolejos.robolejosmobileandroid.services.subject.firebace.FirebaseSubjectService;
import robolejos.robolejosmobileandroid.services.user.IUserService;
import robolejos.robolejosmobileandroid.services.user.firebase.FirebaseUserService;

/**
 * Created by Tim17 on 05-05-2017.
 */

public class ServiceProvider {

    private static ServiceProvider instance;

    //List of the services.
    public IAuthService AuthService;
    public IUserService UserService;
    public ISubjectService SubjectService;
    public ICategoryService CategoryService;
    public IPostService PostService;
    public IReplyService ReplyService;
    public IStorageService StorageService;

    /**
     * Private constructor, for singletone support.
     */
    private ServiceProvider() { }

    private void initialize() {
        //Initialize the services.
        this.UserService = new FirebaseUserService();
        this.AuthService = new FirebaseAuthService();
        this.StorageService = new FirebaseStorageService();
        this.SubjectService = new FirebaseSubjectService();
        this.CategoryService = new FirebaseCategoryService();
        this.PostService = new FirebasePostService();
        this.ReplyService = new FirebaseReplyService();
    }

    /**
     * Gets the instance of the ServiceProvider.
     * @return instance of ServiceProvider.
     */
    public static ServiceProvider getInstance() {
        if (instance == null) {
            instance = new ServiceProvider();
            instance.initialize();
        }
        return instance;
    }
}
