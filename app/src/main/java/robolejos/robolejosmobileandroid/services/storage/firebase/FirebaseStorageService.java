package robolejos.robolejosmobileandroid.services.storage.firebase;

import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Tasks;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.annotations.NonNull;
import robolejos.robolejosmobileandroid.services.storage.IStorageService;

/**
 * Created by Tim17 on 16-05-2017.
 */

public class FirebaseStorageService implements IStorageService {

    //Firebase reference.
    private FirebaseStorage firebaseStorage;

    public FirebaseStorageService() {
        this.firebaseStorage = FirebaseStorage.getInstance();
    }

    @Override
    public Observable<List<String>> upload(final List<Bitmap> files, final List<String> fileNames, final String path) {
        return Observable.create(new ObservableOnSubscribe<List<String>>() {
            @Override
            public void subscribe(final @NonNull ObservableEmitter<List<String>> e) throws Exception {
                StorageReference ref = firebaseStorage.getReference(path);

                if (files.size() == 0)
                    e.onComplete();

                ByteArrayOutputStream baos = new ByteArrayOutputStream();

                List<UploadTask> tasks = new ArrayList<UploadTask>();
                final List<String> results = new ArrayList<String>();

                for (int i = 0; i < files.size(); i++) {
                    Bitmap file = files.get(i);
                    final String fileName = fileNames.get(i);

                    file.compress(Bitmap.CompressFormat.JPEG, 20, baos);
                    byte[] data = baos.toByteArray();
                    baos.reset();

                    UploadTask uploadTask = ref.child(fileName).putBytes(data);
                    uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            //Successfull upload of file.
                            results.add(fileName);
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@android.support.annotation.NonNull Exception ex) {
                            //Failed to upload.
                            e.onError(ex);
                        }
                    });

                    tasks.add(uploadTask);
                }
                baos.close();

                Tasks.whenAll(tasks).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        //Hacky way to notify subscribers that we are finish.
                        e.onNext(results);
                        Log.e("Done", "Images uploaded!!!");
                    }
                });
            }
        });
    }
}
