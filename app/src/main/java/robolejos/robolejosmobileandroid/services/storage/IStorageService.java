package robolejos.robolejosmobileandroid.services.storage;

import android.graphics.Bitmap;
import android.net.Uri;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by Tim17 on 16-05-2017.
 */

public interface IStorageService {

    /**
     * Upload the list of images.
     * @param files - List of files to upload.
     * @param path - Path of the place to upload.
     * @return Observable.
     */
    Observable<List<String>> upload(List<Bitmap> files, List<String> fileNames, String path);
}
