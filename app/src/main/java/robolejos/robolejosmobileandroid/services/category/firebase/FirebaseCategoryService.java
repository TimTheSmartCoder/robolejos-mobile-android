package robolejos.robolejosmobileandroid.services.category.firebase;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.annotations.NonNull;
import robolejos.robolejosmobileandroid.entities.Category;
import robolejos.robolejosmobileandroid.entities.Subject;
import robolejos.robolejosmobileandroid.services.category.ICategoryService;

/**
 * Created by Morten on 09-05-2017.
 */

public class FirebaseCategoryService implements ICategoryService {

    private FirebaseDatabase firebaseDatabase;
    private static final String CATEGORIES_REFERENCE = "categories";

    public FirebaseCategoryService(){
        this.firebaseDatabase = FirebaseDatabase.getInstance();
    }

    @Override
    public Observable<List<Category>> getAllCategories(){
        return Observable.create(new ObservableOnSubscribe<List<Category>>() {
            @Override
            public void subscribe(final @NonNull ObservableEmitter<List<Category>> e) throws Exception {
                DatabaseReference ref = firebaseDatabase.getReference(CATEGORIES_REFERENCE + "/");
                ref.addListenerForSingleValueEvent(new ValueEventListener() {

                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        List<Category> categories = new ArrayList<Category>();
                        for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                            Category category = snapshot.getValue(Category.class);
                            categories.add(category);
                        }
                        Collections.sort(categories, new Comparator<Category>() {
                            @Override
                            public int compare(Category o1, Category o2) {
                                return o1.getTitle().compareTo(o2.getTitle());
                            }
                        });
                        e.onNext(categories);
                        e.onComplete();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        e.onError(databaseError.toException());
                    }
                });
            }
        });
    }
}
