package robolejos.robolejosmobileandroid.services.category;

import java.util.List;

import io.reactivex.Observable;
import robolejos.robolejosmobileandroid.entities.Category;


/**
 * Created by Morten on 09-05-2017.
 */

public interface ICategoryService {

    Observable<List<Category>> getAllCategories();
}
