package robolejos.robolejosmobileandroid.services.reply;

import java.util.List;

import io.reactivex.Observable;
import robolejos.robolejosmobileandroid.entities.Reply;

/**
 * Created by Tim17 on 12-05-2017.
 */

public interface IReplyService {

    /**
     * Gets all the replies related tot the given post uid.
     * @param postUid - Uid of the post to get all replies from.
     * @return List of replies which belongs to the given post uid.
     */
    Observable<List<Reply>> get(String postUid);

    /**
     * Creates a reply out from the given content and uid.
     * @param content - Content of the reply.
     * @param postUid - Uid of the post the reply belongs to.
     * @return Observable of state.
     */
    Observable<Boolean> create(String content, String postUid);
}
