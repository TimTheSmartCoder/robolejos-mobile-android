package robolejos.robolejosmobileandroid.services.reply.firebase;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import robolejos.robolejosmobileandroid.entities.Post;
import robolejos.robolejosmobileandroid.entities.Reply;
import robolejos.robolejosmobileandroid.entities.User;
import robolejos.robolejosmobileandroid.services.ServiceProvider;
import robolejos.robolejosmobileandroid.services.auth.IAuthService;
import robolejos.robolejosmobileandroid.services.post.IPostService;
import robolejos.robolejosmobileandroid.services.reply.IReplyService;
import robolejos.robolejosmobileandroid.services.user.IUserService;

/**
 * Created by Tim17 on 12-05-2017.
 */

public class FirebaseReplyService implements IReplyService {

    private static final String REPLIES_REFERENCE = "replies/";

    //Services.
    private IAuthService authService;

    //Firebase instance.
    private FirebaseDatabase firebaseDatabase;

    public FirebaseReplyService() {
        this.firebaseDatabase = FirebaseDatabase.getInstance();
        this.authService = ServiceProvider.getInstance().AuthService;
    }



    @Override
    public Observable<List<Reply>> get(final String postUid) {
        return Observable.create(new ObservableOnSubscribe<List<Reply>>() {
            @Override
            public void subscribe(final @NonNull ObservableEmitter<List<Reply>> e) throws Exception {
                DatabaseReference ref = firebaseDatabase.getReference(REPLIES_REFERENCE + "/" + postUid);
                ref.orderByChild("username").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        List<Reply> replies = new ArrayList<Reply>();
                        for (DataSnapshot replySnapshot : dataSnapshot.getChildren()) {
                            replies.add(replySnapshot.getValue(Reply.class));
                        }
                        e.onNext(replies);
                        e.onComplete();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        e.onError(databaseError.toException());
                    }
                });
            }
        });
    }

    @Override
    public Observable<Boolean> create(final String content, final String postUid) {
        return authService.getCurrentUser().switchMap(new Function<User, ObservableSource<? extends Boolean>>() {
            @Override
            public ObservableSource<? extends Boolean> apply(@NonNull User user) throws Exception {
                final Reply reply = new Reply();
                reply.info.content = content;
                reply.info.username = user.profile.username;
                reply.info.userUid = user.uid;
                return Observable.create(new ObservableOnSubscribe<Boolean>() {
                    @Override
                    public void subscribe(final @NonNull ObservableEmitter<Boolean> e) throws Exception {
                        DatabaseReference ref = firebaseDatabase.getReference(REPLIES_REFERENCE + "/" + postUid);
                        HashMap<String, Object> data = new HashMap<String, Object>();
                        data.put("info", reply.info);
                        ref.push().setValue(data, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                if (databaseError == null) {
                                    e.onNext(true);
                                    e.onComplete();
                                } else {
                                    e.onError(databaseError.toException());
                                }
                            }
                        });
                    }
                });
            }
        });
    }
}
