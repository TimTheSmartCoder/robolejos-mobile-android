package robolejos.robolejosmobileandroid.services.user.listeners;

import robolejos.robolejosmobileandroid.entities.User;

/**
 * Created by Tim17 on 05-05-2017.
 */

public interface IUserGetListener {

    /**
     * Listener which is called on success of an
     * user action.
     * @param user - User from action.
     */
    void onSuccess(User user);

    /**
     * Listener which is called on error of
     * an user action.
     * @param error
     */
    void onError(String error);
}
