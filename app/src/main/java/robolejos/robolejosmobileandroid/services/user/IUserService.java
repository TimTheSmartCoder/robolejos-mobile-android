package robolejos.robolejosmobileandroid.services.user;

import io.reactivex.Observable;
import robolejos.robolejosmobileandroid.entities.User;
import robolejos.robolejosmobileandroid.services.user.listeners.IUserGetListener;

/**
 * Created by Tim17 on 05-05-2017.
 */

public interface IUserService {

    /**
     * Gets the user with the given uid.
     * @param uid - Uid of the user to get.
     */
    Observable<User> get(String uid);
}
