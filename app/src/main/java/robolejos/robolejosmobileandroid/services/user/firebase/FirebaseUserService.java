package robolejos.robolejosmobileandroid.services.user.firebase;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.annotations.NonNull;
import robolejos.robolejosmobileandroid.entities.User;
import robolejos.robolejosmobileandroid.services.user.IUserService;
import robolejos.robolejosmobileandroid.services.user.listeners.IUserGetListener;

/**
 * Created by Tim17 on 05-05-2017.
 */

public class FirebaseUserService implements IUserService {

    private static final String USER_REFERENCE = "users";
    private FirebaseDatabase firebaseDatabase;

    public FirebaseUserService() {
        this.firebaseDatabase = FirebaseDatabase.getInstance();
    }

    @Override
    public Observable<User> get(final String uid) {
        return Observable.create(new ObservableOnSubscribe<User>() {
            @Override
            public void subscribe(final @NonNull ObservableEmitter<User> e) throws Exception {
                DatabaseReference ref = firebaseDatabase.getReference(USER_REFERENCE + "/" + uid);
                ref.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user = dataSnapshot.getValue(User.class);
                        e.onNext(user);
                        e.onComplete();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        e.onError(databaseError.toException());
                    }
                });
            }
        });
    }
}
