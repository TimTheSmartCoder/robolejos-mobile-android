package robolejos.robolejosmobileandroid.services.post;

import android.graphics.Bitmap;
import android.net.Uri;

import java.util.List;

import io.reactivex.Observable;
import robolejos.robolejosmobileandroid.entities.Post;

/**
 * Created by Morten on 10-05-2017.
 */

public interface IPostService {
    Observable<String> createPost(String content, String title, String subjectUid);

    Observable<List<Post>> getSelectedPost(String subjectUid);

    /**
     * Gets a post out from the given post uid.
     * @param uid - Uid of the post to get.
     * @return Observable.
     */
    Observable<Post> get(String uid);

    /**
     * Gets the images belonging to the post.
     * @param uid - Uid of the post to get images for.
     * @return Observable with image urls.
     */
    Observable<Uri> getImagesDownloadUrl(String uid);

    /**
     * Uploads the given images to the storage and
     * place it under the given id.
     * @param images - List of images to upload.
     * @param uid - Uid of the post the images is uploaded to.
     * @return Observable.
     */
    Observable<Boolean> uploadImages(List<Bitmap> images, String uid);
}
