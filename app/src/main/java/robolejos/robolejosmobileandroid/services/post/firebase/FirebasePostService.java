package robolejos.robolejosmobileandroid.services.post.firebase;

import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import robolejos.robolejosmobileandroid.entities.Post;
import robolejos.robolejosmobileandroid.entities.User;
import robolejos.robolejosmobileandroid.services.ServiceProvider;
import robolejos.robolejosmobileandroid.services.auth.IAuthService;
import robolejos.robolejosmobileandroid.services.post.IPostService;
import robolejos.robolejosmobileandroid.services.storage.IStorageService;

/**
 * Created by Morten on 10-05-2017.
 */

public class FirebasePostService implements IPostService {

    //Constants.
    private static final String POSTS_REFERENCE = "posts";

    //Firebase references.
    private FirebaseDatabase firebaseDatabase;
    private FirebaseStorage firebaseStorage;

    //Services.
    private IStorageService storageService;
    private IAuthService authService;

    public FirebasePostService(){
        this.firebaseDatabase = FirebaseDatabase.getInstance();
        this.firebaseStorage = FirebaseStorage.getInstance();
        this.storageService = ServiceProvider.getInstance().StorageService;
        this.authService = ServiceProvider.getInstance().AuthService;
    }


    /**
     * Creates a new post by combining two observables
     * and sending a true or false reply back.
     * @param content
     * @param title
     * @param subjectUid
     * @return
     */
    @Override
    public Observable<String> createPost(final String content, final String title, final String subjectUid) {
        return authService.getCurrentUser().switchMap(new Function<User, ObservableSource<? extends String>>() {
            @Override
            public ObservableSource<? extends String> apply(@NonNull User user) throws Exception {
                final Post post = new Post();
                post.info.content = content;
                post.info.title = title;
                post.info.username = user.profile.username;
                post.info.userUid = user.uid;
                post.info.subjectUid = subjectUid;
                return Observable.create(new ObservableOnSubscribe<String>() {
                    @Override
                    public void subscribe(final @NonNull ObservableEmitter<String> e) throws Exception {
                        DatabaseReference ref = firebaseDatabase.getReference(POSTS_REFERENCE + "/").push();
                        final String postuid = ref.getKey();
                        HashMap<String, Object> data = new HashMap<String, Object>();
                        data.put("info", post.info);
                        ref.setValue(data, new DatabaseReference.CompletionListener() {
                            @Override
                            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                                if (databaseError == null){
                                    e.onNext(postuid);
                                    e.onComplete();
                                }
                                else{
                                    e.onError(databaseError.toException());
                                }
                            }
                        });
                    }
                });
            };
        });
    }

    /**
     * returns a list of posts belonging to a sertend subjecct
     * @param subjectUid
     * @return
     */
    @Override
    public Observable<List<Post>> getSelectedPost(final String subjectUid) {
        return Observable.create(new ObservableOnSubscribe<List<Post>>() {
            @Override
            public void subscribe(final @NonNull ObservableEmitter<List<Post>> e) throws Exception {
                DatabaseReference ref = firebaseDatabase.getReference(POSTS_REFERENCE + "/");
                ref.orderByChild("info/subjectUid").equalTo(subjectUid).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        List<Post> posts = new ArrayList<Post>();
                        for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                            Post post = postSnapshot.getValue(Post.class);
                            posts.add(post);
                        }
                        Collections.sort(posts, new Comparator<Post>() {
                            @Override
                            public int compare(Post o1, Post o2) {
                                return o1.info.title.compareTo(o2.info.title);
                            }
                        });
                        Log.e("ServicePostCount", "" + posts.size());
                        e.onNext(posts);
                        e.onComplete();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        e.onError(databaseError.toException());
                    }
                });
            }
        });

    }

    /**
     * Gets a specific post based on its uid
     * @param uid - Uid of the post to get.
     * @return
     */
    @Override
    public Observable<Post> get(final String uid) {
        return Observable.create(new ObservableOnSubscribe<Post>() {
            @Override
            public void subscribe(final @NonNull ObservableEmitter<Post> e) throws Exception {
                DatabaseReference ref = firebaseDatabase.getReference(POSTS_REFERENCE + "/" + uid);
                ref.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Post post = dataSnapshot.getValue(Post.class);

                        //We are removing the first item in the image list
                        //because for some reason firebase is returning a
                        //null object as the first item.
                        //TODO: Research why this is happening and maybe fix the issue.
                        if(post.info.images != null && post.info.images.size() != 0){
                            //Log.e("found", post.info.images.get(0));
                            //post.info.images.remove(0);
                        }

                        e.onNext(post);
                        e.onComplete();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        e.onError(databaseError.toException());
                    }
                });
            }
        });
    }

    @Override
    public Observable<Uri> getImagesDownloadUrl(final String uid) {
        return this.get(uid).switchMap(new Function<Post, ObservableSource<? extends Uri>>() {
            @Override
            public ObservableSource<? extends Uri> apply(final @NonNull Post post) throws Exception {
                return Observable.create(new ObservableOnSubscribe<Uri>() {
                    @Override
                    public void subscribe(final @NonNull ObservableEmitter<Uri> e) throws Exception {
                        StorageReference ref = firebaseStorage.getReference(POSTS_REFERENCE + "/" + uid + "/");
                        if(post.info.images != null) {
                            for (String image : post.info.images) {
                                ref.child(image).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                    @Override
                                    public void onSuccess(Uri uri) {
                                        e.onNext(uri);
                                    }
                                });
                            }
                        }
                        else
                            e.onComplete();
                    }
                });
            }
        });
    }

    @Override
    public Observable<Boolean> uploadImages(final List<Bitmap> images, String uid) {
        String path = POSTS_REFERENCE + "/" + uid;
        final DatabaseReference ref = firebaseDatabase.getReference(POSTS_REFERENCE + "/" + uid + "/info/images/");

        //Create numeric file names for the images.
        final List<String> fileNames = new ArrayList<>();
        for(int i = 0; i < images.size(); i++)
            fileNames.add(String.valueOf(i));

        return this.storageService.upload(images, fileNames, path).switchMap(new Function<List<String>, ObservableSource<? extends Boolean>>() {
            @Override
            public ObservableSource<? extends Boolean> apply(@NonNull List<String> strings) throws Exception {
                return Observable.create(new ObservableOnSubscribe<Boolean>() {
                    @Override
                    public void subscribe(final @NonNull ObservableEmitter<Boolean> e) throws Exception {
                        HashMap<String, Object> data = new HashMap<String, Object>();

                        for(String fileName : fileNames) {
                            data.put(fileName, fileName);
                        }

                        ref.updateChildren(data).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                e.onNext(true);
                                e.onComplete();
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@android.support.annotation.NonNull Exception ex) {
                                e.onError(ex);
                            }
                        });
                    }
                });
            }
        });
    }
}
