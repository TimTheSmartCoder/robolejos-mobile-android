package robolejos.robolejosmobileandroid.services.subject;

import java.util.List;

import io.reactivex.Observable;
import robolejos.robolejosmobileandroid.entities.Category;
import robolejos.robolejosmobileandroid.entities.Subject;
/**
 * Created by Morten on 08-05-2017.
 */

public interface ISubjectService {
    Observable<List<Subject>> getSubjects();
    Observable<List<Subject>> getSelectedSubjects(String categoryUid);
    Observable<Subject> getOneSelectedSubject(String uid);
}
