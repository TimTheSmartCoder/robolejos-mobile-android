package robolejos.robolejosmobileandroid.services.subject.firebace;


import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.annotations.NonNull;
import robolejos.robolejosmobileandroid.entities.Subject;
import robolejos.robolejosmobileandroid.services.subject.ISubjectService;

/**
 * Created by Morten on 08-05-2017.
 */

public class FirebaseSubjectService implements ISubjectService {

    private FirebaseDatabase firebaseDatabase;

    private static final String SUBJECT_REFERENCE = "subjects";

    public FirebaseSubjectService(){
        this.firebaseDatabase = FirebaseDatabase.getInstance();
    }

    @Override
    public Observable<List<Subject>> getSubjects(){
        return Observable.create(new ObservableOnSubscribe<List<Subject>>() {
            @Override
            public void subscribe(final @NonNull ObservableEmitter<List<Subject>> e) throws Exception {
                DatabaseReference ref = firebaseDatabase.getReference(SUBJECT_REFERENCE + "/");
                ref.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot snapshot) {
                        List<Subject> subjects = new ArrayList<Subject>();
                        for (DataSnapshot subjectSnapshot: snapshot.getChildren()) {
                            Subject subject = subjectSnapshot.getValue(Subject.class);
                            subjects.add(subject);
                        }
                        Collections.reverse(subjects);
                        e.onNext(subjects);
                        e.onComplete();
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        e.onError(databaseError.toException());
                    }
                });
            }
        });
    }

    @Override
    public Observable<List<Subject>> getSelectedSubjects(final String categoryUid){
        return Observable.create(new ObservableOnSubscribe<List<Subject>>() {
            @Override
            public void subscribe(final @NonNull ObservableEmitter<List<Subject>> e) throws Exception {
                DatabaseReference ref = firebaseDatabase.getReference(SUBJECT_REFERENCE + "/");
                ref.orderByChild("categoryUid").equalTo(categoryUid).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        List<Subject> subjects = new ArrayList<Subject>();

                        for (DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                            Subject subject = postSnapshot.getValue(Subject.class);
                            subjects.add(subject);
                        }
                        Collections.sort(subjects, new Comparator<Subject>() {
                            @Override
                            public int compare(Subject o1, Subject o2) {
                                return o1.title.compareTo(o2.title);
                            }
                        });

                        e.onNext(subjects);
                        e.onComplete();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        e.onError(databaseError.toException());
                    }
                });
            }
        });
    }

    @Override
    public Observable<Subject> getOneSelectedSubject(final String uid) {
        return Observable.create(new ObservableOnSubscribe<Subject>() {
            @Override
            public void subscribe(final @NonNull ObservableEmitter<Subject> e) throws Exception {
                DatabaseReference ref = firebaseDatabase.getReference(SUBJECT_REFERENCE + "/" + uid);
                ref.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Subject subject = new Subject();
                        subject = dataSnapshot.getValue(Subject.class);

                        e.onNext(subject);
                        e.onComplete();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        e.onError(databaseError.toException());
                    }
                });
            }
        });
    }
}
