package robolejos.robolejosmobileandroid.services.auth;

import android.app.Activity;

import io.reactivex.Observable;
import robolejos.robolejosmobileandroid.entities.User;

/**
 * Created by Tim17 on 05-05-2017.
 */

public interface IAuthService {

    /**
     * Checks if the user is authenticated.
     */
    boolean isAuthenticated();

    /**
     * Gets the current authenticated user.
     */
    Observable<User> getCurrentUser();

    /**
     * Authenticates the user with the given e-mail and
     * password.
     * @param email - E-mail to sign in user with.
     * @param password - Password to sign in user with.
     */
    Observable authenticate(final Activity activity, final String email, final String password);

    /**
     * Signs out the authenticated user.
     */
    void signOut();
}
