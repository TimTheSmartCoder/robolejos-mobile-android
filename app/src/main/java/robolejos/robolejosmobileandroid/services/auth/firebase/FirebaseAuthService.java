package robolejos.robolejosmobileandroid.services.auth.firebase;

import android.app.Activity;
import android.support.annotation.NonNull;

import com.google.android.gms.common.data.DataBufferObserver;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import robolejos.robolejosmobileandroid.entities.User;
import robolejos.robolejosmobileandroid.services.ServiceProvider;
import robolejos.robolejosmobileandroid.services.auth.IAuthService;
import robolejos.robolejosmobileandroid.services.user.IUserService;

/**
 * Created by Tim17 on 05-05-2017.
 */

public class FirebaseAuthService implements IAuthService {

    /**
     * Instance of the FirebaseAuth system.
     */
    private FirebaseAuth firebaseAuth;

    /**
     * Instance of the UserService.
     */
    private IUserService userService;

    public FirebaseAuthService() {
        //Get the instance of the firebase authentication.
        this.firebaseAuth = FirebaseAuth.getInstance();
        this.userService = ServiceProvider.getInstance().UserService;;
    }

    @Override
    public boolean isAuthenticated() {
        //If firebase getCurrentUser returns null, the user is
        //not signed in.
        return this.firebaseAuth.getCurrentUser() != null;
    }

    @Override
    public Observable<User> getCurrentUser() {
        FirebaseUser firebaseUser = this.firebaseAuth.getCurrentUser();
        return this.userService.get(firebaseUser.getUid());
    }

    @Override
    public Observable authenticate(final Activity activity, final String email, final String password) {
        return Observable.create(new ObservableOnSubscribe() {
            @Override
            public void subscribe(final @io.reactivex.annotations.NonNull ObservableEmitter e) throws Exception {
                firebaseAuth
                        .signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                               if (task.isSuccessful()) {
                                   e.onComplete();
                               } else {
                                   e.onError(task.getException());
                               }
                            }
                        });
            }
        });
    }

    @Override
    public void signOut() {
        this.firebaseAuth.signOut();
    }
}
