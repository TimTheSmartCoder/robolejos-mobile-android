package robolejos.robolejosmobileandroid;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.SerialDisposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import robolejos.robolejosmobileandroid.adapters.ImageGridViewAdapter;
import robolejos.robolejosmobileandroid.adapters.ImageUrlGridViewAdapter;
import robolejos.robolejosmobileandroid.adapters.ReplyAdapter;
import robolejos.robolejosmobileandroid.entities.*;
import robolejos.robolejosmobileandroid.entities.Post;
import robolejos.robolejosmobileandroid.services.ServiceProvider;
import robolejos.robolejosmobileandroid.services.auth.IAuthService;
import robolejos.robolejosmobileandroid.services.post.IPostService;
import robolejos.robolejosmobileandroid.services.reply.IReplyService;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PostView.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PostView#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PostView extends Fragment implements View.OnClickListener {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_POST = "postUid";
    private String postUid;

    //Services.
    private IReplyService replyService;
    private IAuthService authService;
    private IPostService postService;

    //Reference components.
    private ListView repliesListView;
    private EditText replyEditText;
    private Button replyButton;
    private LinearLayout contentLayout;
    private LinearLayout loadingLayout;
    private TextView titleEditText;
    private EditText contentEditText;
    private TextView usernameTextView;
    private GridView imageGridView;
    private TextView createdTextView;

    //Subscriptions.
    private Disposable postSubscription;
    private Disposable repliesSubscription;
    private Disposable repliesCreateSubscription;
    private Disposable imageSubscription;

    private OnFragmentInteractionListener mListener;

    public PostView() { }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param postUid Id of the post to view.
     * @return A new instance of fragment PostView.
     */
    public static PostView newInstance(String postUid) {
        PostView fragment = new PostView();
        Bundle args = new Bundle();
        args.putString(ARG_POST, postUid);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            postUid = getArguments().getString(ARG_POST);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_post_view, container, false);
        this.repliesListView = (ListView) view.findViewById(R.id.repliesListView);
        this.replyEditText = (EditText) view.findViewById(R.id.replyEditText);
        this.replyButton = (Button) view.findViewById(R.id.replyButton);
        this.contentLayout = (LinearLayout) view.findViewById(R.id.contentLayout);
        this.loadingLayout = (LinearLayout) view.findViewById(R.id.loadingLayout);
        this.contentEditText = (EditText) view.findViewById(R.id.contentEditText);
        this.titleEditText = (TextView) view.findViewById(R.id.titleTextView);
        this.usernameTextView = (TextView) view.findViewById(R.id.usernameTextView);
        this.imageGridView = (GridView) view.findViewById(R.id.imageGridView);
        this.createdTextView = (TextView) view.findViewById(R.id.createdTextView);
        loading(true);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        disposeSubscriptions();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //Setup grid view.
        imageGridView.setAdapter(new ImageUrlGridViewAdapter(getContext()));

        //Services.
        this.replyService = ServiceProvider.getInstance().ReplyService;
        this.authService = ServiceProvider.getInstance().AuthService;
        this.postService = ServiceProvider.getInstance().PostService;

        //Attach click listeners.
        this.replyButton.setOnClickListener(this);

        //Get information from firebase.
        fetch();
    }

    /**
     * Fetches information to update the replies list.
     */
    private void fetch() {

        //Show the state of loading.
        this.loading(true);

        //Dispose running subscriptions.
        disposeSubscriptions();

        //Get post information.
        this.postSubscription
                = this.postService.get(postUid).subscribe(new Consumer<Post>() {
            @Override
            public void accept(final @NonNull Post post) throws Exception {
                if (postSubscription != null && !postSubscription.isDisposed()) {
                    titleEditText.setText(post.info.title);
                    contentEditText.setText(post.info.content);
                    usernameTextView.setText(post.info.username);
                    createdTextView.setText(new Timestamp(post.created).toString());
                    repliesSubscription = replyService.get(postUid).subscribe(new Consumer<List<Reply>>() {
                        @Override
                        public void accept(@NonNull List<Reply> replies) throws Exception {
                            ReplyAdapter replyAdapter = new ReplyAdapter(getContext(), R.layout.resource_replyitem, replies);
                            repliesListView.setAdapter(replyAdapter);
                            loading(false);
                        }
                    });
                }
            }
        });

        ((ImageUrlGridViewAdapter)imageGridView.getAdapter()).clear();

        this.imageSubscription = this.postService.getImagesDownloadUrl(postUid).subscribe(new Consumer<Uri>() {
            @Override
            public void accept(@NonNull Uri uri) throws Exception {
                ((ImageUrlGridViewAdapter)imageGridView.getAdapter()).addImage(uri);
            }
        });
    }

    /**
     * Click on reply button (Creates a reply to the post.).
     * @param view
     */
    private void createReply(View view) {
        final String content = this.replyEditText.getText().toString();

        if (content == null || content.length() <= 0) {
            this.replyEditText.setError("Please fill in content.");
            return;
        }

        //Set to loading.
        this.loading(true);

        this.repliesCreateSubscription
                = this.replyService.create(content, postUid).subscribe(new Consumer<Boolean>() {
            @Override
            public void accept(@NonNull Boolean aBoolean) throws Exception {
                fetch();
                replyEditText.setText("");
            }
        });
    }

    private void disposeSubscriptions() {
        //Unsubscripe from the observables.
        if (this.postSubscription != null && !this.postSubscription.isDisposed())
            this.postSubscription.dispose();
        if (this.repliesSubscription != null && !this.repliesSubscription.isDisposed())
            this.repliesSubscription.dispose();
        if (this.repliesCreateSubscription != null && !this.repliesCreateSubscription.isDisposed())
            this.repliesCreateSubscription.dispose();
        if (this.imageSubscription != null && !this.imageSubscription.isDisposed())
            this.imageSubscription.dispose();
    }

    private void loading(boolean state) {
        if (state) {
            this.contentLayout.setVisibility(View.INVISIBLE);
            this.loadingLayout.setVisibility(View.VISIBLE);
        } else {
            this.loadingLayout.setVisibility(View.INVISIBLE);
            this.contentLayout.setVisibility(View.VISIBLE);
        }
    }
    /**
     * OnClick method for handling the event from the
     * layout. Normally it will be send to the Activity,
     * but in our case it will not cause we attach this
     * function through code and it will when call the
     * correct method.
     * @param v - View calling.
     */
    @Override
    public void onClick(View v) {
        Log.w("ww", "Reply created!!!!!!");
        switch (v.getId()) {
            case R.id.replyButton:
                Log.w("ww", "Reply created!!!!!!");
                createReply(v);
                break;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
