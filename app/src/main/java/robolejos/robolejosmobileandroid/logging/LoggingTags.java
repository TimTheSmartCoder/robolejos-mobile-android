package robolejos.robolejosmobileandroid.logging;

/**
 * Created by Tim17 on 05-05-2017.
 */

public class LoggingTags {
    public static final String FIREBASE = "Firebase";
    public static final String Authentication = "Authentication";
    public static final String CATEGORIES = "categoriess";
    public static final String FORUM = "Forum";
    public static final String PROFILE = "Profile";
    public static final String SUBJECT = "Subject";
    public static final String POST = "post";
}
