package robolejos.robolejosmobileandroid;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import robolejos.robolejosmobileandroid.entities.User;
import robolejos.robolejosmobileandroid.services.ServiceProvider;
import robolejos.robolejosmobileandroid.services.auth.IAuthService;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        Forum.OnForumSubjectSelecten,
        SubjectView.OnSubjectViewSelectedListener,
        Post.OnPostCreated{

    /**
     * View references.
     */
    private View headerView;
    private TextView userNameTextView;
    private TextView userEmailTextView;

    /**
     * Services.
     */
    private IAuthService authService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Auto generated code for Drawble navigationbar begin.
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        //Auto generated code for Drawble navigationbar end.

        /**
         * Amateur coding begins ....
         */
        getComponents(navigationView);
        getServices();
        fetchUserInformation();
        setHomePage();
    }

    /**
     * Sets the homepage of the main acitivity.
     */
    private void setHomePage() {
        Fragment fragment = Forum.newInstance();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, fragment);
        ft.commit();
    }

    /**
     * Get the components from the navigation view.
     * @param navigationView - NavigationView to get components from.
     */
    private void getComponents(NavigationView navigationView) {
        this.headerView = navigationView.getHeaderView(0);
        this.userNameTextView = (TextView) this.headerView.findViewById(R.id.userNameTextView);
        this.userEmailTextView = (TextView) this.headerView.findViewById(R.id.userEmailTextView);
    }

    /**
     * Gets the required services.
     */
    private void getServices() {
        this.authService = ServiceProvider.getInstance().AuthService;
    }

    /**
     * Fetches user information from the database, and
     * displays it in the ui.
     */
    private void fetchUserInformation() {
        this.authService.getCurrentUser().subscribe(new Observer<User>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) { }

            @Override
            public void onNext(@NonNull User user) {
                userNameTextView.setText(user.profile.username);
                userEmailTextView.setText(user.profile.email);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                userNameTextView.setText("Unknown");
                userEmailTextView.setText("Unknown");
            }

            @Override
            public void onComplete() { }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        Fragment fragment = null;

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_profile) {
            fragment = Profile.newInstance();
        } else if (id == R.id.nav_forum) {
            fragment = Forum.newInstance();
        } else if (id == R.id.nav_create_post) {
            fragment = Post.newInstance();
        } else if (id == R.id.nav_logout) {
            this.authService.signOut();
            Intent intent = new Intent(this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_web) {
            Uri webpage = Uri.parse(getString(R.string.website_home));
            Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            } else {
                Toast.makeText(this, "Unable to open your browser, please go to " + getString(R.string.website_home), Toast.LENGTH_LONG).show();
            }
        }

        if (fragment != null) {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.replace(R.id.content_frame, fragment);
            ft.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onSubjectSeleccted(String subjectUid) {
        Fragment fragment = SubjectView.newInstance(subjectUid);

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, fragment);
        ft.commit();
    }

    @Override
    public void onPostCreateClicked( ) {
        Fragment fragment = Post.newInstance();

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, fragment);
        ft.commit();
    }

    @Override
    public void onPostSelectedListener(String postUid) {
        Fragment fragment = PostView.newInstance(postUid);

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, fragment);
        ft.commit();
    }

    @Override
    public void onPostCreatedSeleccted(String postuid) {
        Fragment fragment = PostView.newInstance(postuid);

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, fragment);
        ft.commit();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent resultIntent) {
        super.onActivityResult(requestCode, resultCode, resultIntent);
        Log.e("Image", "activity main");
    }
}
