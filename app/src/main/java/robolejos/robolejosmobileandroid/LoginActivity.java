package robolejos.robolejosmobileandroid;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import robolejos.robolejosmobileandroid.logging.LoggingTags;
import robolejos.robolejosmobileandroid.services.ServiceProvider;
import robolejos.robolejosmobileandroid.services.auth.IAuthService;

public class LoginActivity extends AppCompatActivity {

    private IAuthService authService;

    private EditText emailText;
    private EditText passwordText;
    private TextView errorText;
    private LinearLayout spinnerLayout;
    private LinearLayout loginLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        components();

        //Load the AuthService.
        this.authService = ServiceProvider.getInstance().AuthService;

        this.emailText.setText("morten@mail.dk");
        this.passwordText.setText("123456");
    }

    /**
     * Set up the reference to the components.
     */
    public void components() {
        this.emailText = (EditText) findViewById(R.id.emailText);
        this.passwordText = (EditText) findViewById(R.id.passwordText);
        this.errorText = (TextView) findViewById(R.id.errorText);
        this.spinnerLayout = (LinearLayout) findViewById(R.id.spinnerLayout);
        this.loginLayout = (LinearLayout) findViewById(R.id.loginLayout);
    }

    public void onSignIn(View view) {
        String email = this.emailText.getText().toString();
        String password = this.passwordText.getText().toString();
        this.signIn(email, password);
    }

    public void onRegister(View view){
        Uri webpage = Uri.parse(getString(R.string.website_register));
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        } else {
            Toast.makeText(this, "Unable to open your browser, please go to " + getString(R.string.website_register), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Performs the sign in process to Firebase cloud.
     * @param email - E-mail.
     * @param password - Password.
     */
    public void signIn(final String email, String password) {

        /**
         * Validate if e-mail and password is added.
         * TODO: Maybe move this out to a TextValidator.
         */
        if (email.length() <= 0 || password.length() <= 0) {
            this.errorText.setText("E-mail and password is required.");
            return;
        }

        showSpinner();

        final Activity activity = this;

        this.authService.authenticate(activity, email, password).subscribe(new Observer() {
            @Override
            public void onSubscribe(@NonNull Disposable d) { }
            @Override
            public void onNext(@NonNull Object o) { }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.e(LoggingTags.Authentication, "Authentication failed: " + e.getMessage());
                errorText.setText(e.getMessage());
                hideSpinner();
            }

            @Override
            public void onComplete() {
                Log.d(LoggingTags.Authentication, "Authentication succeeded.");
                Intent intent = new Intent(activity, MainActivity.class);
                startActivity(intent);
            }
        });
    }

    private void showSpinner() {
        loginLayout.setVisibility(View.INVISIBLE);
        spinnerLayout.setVisibility(View.VISIBLE);
    }

    private void hideSpinner() {
        spinnerLayout.setVisibility(View.INVISIBLE);
        loginLayout.setVisibility(View.VISIBLE);
    }
}

