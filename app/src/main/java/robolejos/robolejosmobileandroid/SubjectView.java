package robolejos.robolejosmobileandroid;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import robolejos.robolejosmobileandroid.adapters.PostAdapter;
import robolejos.robolejosmobileandroid.entities.Subject;
import robolejos.robolejosmobileandroid.logging.LoggingTags;
import robolejos.robolejosmobileandroid.services.ServiceProvider;
import robolejos.robolejosmobileandroid.services.post.IPostService;
import robolejos.robolejosmobileandroid.services.post.firebase.FirebasePostService;
import robolejos.robolejosmobileandroid.services.subject.ISubjectService;
import robolejos.robolejosmobileandroid.services.subject.firebace.FirebaseSubjectService;
import robolejos.robolejosmobileandroid.entities.Post;

public class SubjectView extends Fragment {

    private ListView postList;
    private EditText title;
    private EditText maintext;
    private String subjectUid;
    private List<Post> posts;
    private Button createPost;

    private Disposable subjectSubribtion;
    private Disposable postSubribtion;

    private IPostService postService;
    private ISubjectService subjectService;

    private Subject subject;


    private OnSubjectViewSelectedListener onSubjectViewSelectedListener;

    public SubjectView() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.

     * @return A new instance of fragment SubjectView.
     */
    // TODO: Rename and change types and number of parameters
    public static SubjectView newInstance(String subjectuid) {
        SubjectView fragment = new SubjectView();
        Bundle args = new Bundle();
        args.putString("subjectuid", subjectuid);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            subjectUid = getArguments().getString("subjectuid");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_subject_view, container, false);
        this.postList = (ListView) view.findViewById(R.id.postList);
        this.maintext = (EditText) view.findViewById(R.id.maintext);
        this.title = (EditText) view.findViewById(R.id.title);
        this.createPost = (Button) view.findViewById(R.id.postcreate);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof OnSubjectViewSelectedListener){
            this.onSubjectViewSelectedListener = (OnSubjectViewSelectedListener) context;
        }

        fetch();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (this.postSubribtion != null && !this.postSubribtion.isDisposed())
            this.postSubribtion.dispose();
        if (this.subjectSubribtion != null && !this.subjectSubribtion.isDisposed())
            this.subjectSubribtion.dispose();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        this.subjectService = ServiceProvider.getInstance().SubjectService;
        this.postService = ServiceProvider.getInstance().PostService;

        posts = new ArrayList<Post>();

        fetch();
        setPostListener();
        setPostCreateListener();
    }

    private void fetch() {
        if (this.subjectService != null && this.postService != null) {
            getTheSubject(this.subjectUid);
            getPosts();
        }
    }

    private void getTheSubject(String uid) {
        this.subjectSubribtion = this.subjectService.getOneSelectedSubject(uid).subscribe(
                new Consumer<Subject>() {
                    @Override
                    public void accept(@NonNull Subject s) throws Exception {
                        title.setText(s.title);
                        maintext.setText(s.description);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(@NonNull Throwable e) throws Exception {
                        Log.e(LoggingTags.SUBJECT, e.getMessage());
                    }
                });
    }

    private void getPosts() {
        this.postSubribtion = this.postService.getSelectedPost(this.subjectUid).subscribe(new Consumer<List<Post>>() {
            @Override
            public void accept(@NonNull List<Post> p) throws Exception {
                posts = p;
                setPosts();
            }
        }, new Consumer<Throwable>() {
            @Override
            public void accept(@NonNull Throwable e) throws Exception {
                Log.e(LoggingTags.SUBJECT, e.getMessage());
            }
        });

    }

    private void setPosts(){
        final Fragment fragment = this;
        PostAdapter adapter = new PostAdapter(fragment.getContext(), R.layout.resource_postitem, posts);
        postList.setAdapter(adapter);
    }

    private void setPostListener() {
        this.postList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object object = parent.getItemAtPosition(position);
                Post post = (Post) object;
                if(post != null){
                    onSubjectViewSelectedListener.onPostSelectedListener(post.uid);
                }
            }
        });
    }

    private void setPostCreateListener() {
        this.createPost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onSubjectViewSelectedListener.onPostCreateClicked();
            }
        });
    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnSubjectViewSelectedListener {
        // TODO: Update argument type and name
        void onPostSelectedListener(String postUid);
        void onPostCreateClicked();
    }
}
